# ModularAppModal

[![CI Status](http://img.shields.io/travis/Wills Ward/ModularAppModal.svg?style=flat)](https://travis-ci.org/Wills Ward/ModularAppModal)
[![Version](https://img.shields.io/cocoapods/v/ModularAppModal.svg?style=flat)](http://cocoapods.org/pods/ModularAppModal)
[![License](https://img.shields.io/cocoapods/l/ModularAppModal.svg?style=flat)](http://cocoapods.org/pods/ModularAppModal)
[![Platform](https://img.shields.io/cocoapods/p/ModularAppModal.svg?style=flat)](http://cocoapods.org/pods/ModularAppModal)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ModularAppModal is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ModularAppModal"
```

## Author

Wills Ward, wward@warddevelopment.com

## License

ModularAppModal is available under the MIT license. See the LICENSE file for more info.
