#
# Be sure to run `pod lib lint ModularAppModal.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ModularAppModal"
  s.version          = "1.0.0"
  s.summary          = "A demo of the modular app concept. This app will display a modal view when invoked."
  s.homepage         = "http://stash.warddevelopment.com/projects/COC/repos/modular-app-modal/browse"
  s.license          = 'MIT'
  s.author           = { "Wills Ward" => "wward@warddevelopment.com" }
  s.source           = { :git => "http://stash.warddevelopment.com/scm/coc/modular-app-modal.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'ModularAppModal' => ['Pod/Assets/**/*.{storyboard,png,jpeg,xib}']
  }

  s.frameworks = 'UIKit'
  s.dependency 'ModularAppCore', '~> 1.0.0'
end
