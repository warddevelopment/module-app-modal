//
//  MAModalApp.m
//  Pods
//
//  Created by Wills Ward on 6/24/15.
//
//

#import <UIKit/UIKit.h>
#import "MAModalApp.h"
#import "MAAppViewController.h"

@implementation MAModalApp

- (NSString*)name {
	return @"Modal App";
}

- (void)invoke {
	NSBundle* bundle = [NSBundle bundleForClass:NSClassFromString(@"MAModalApp")];
	UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:bundle];
	MAAppViewController* controller = [storyboard instantiateInitialViewController];
	UIWindow* window = [[UIApplication sharedApplication] windows][0];

	if (window) {
		[[window rootViewController] presentViewController:controller animated:YES completion:nil];
	} else {
		NSLog(@"ERROR: No window to which to add the view controller.");
	}
}

@end
