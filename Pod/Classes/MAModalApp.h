//
//  MAModalApp.h
//  Pods
//
//  Created by Wills Ward on 6/24/15.
//
//

#import <Foundation/Foundation.h>
#import "MAApp.h"

@interface MAModalApp : NSObject <MAApp>

@end
