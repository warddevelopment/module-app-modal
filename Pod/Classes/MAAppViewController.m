//
//  MAAppViewController.m
//  Pods
//
//  Created by Wills Ward on 6/24/15.
//
//

#import "MAAppViewController.h"

@implementation MAAppViewController

- (IBAction)doneButtonPressed:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
