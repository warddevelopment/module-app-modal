//
//  main.m
//  ModularAppModal
//
//  Created by Wills Ward on 06/24/2015.
//  Copyright (c) 2014 Wills Ward. All rights reserved.
//

@import UIKit;
#import "MAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MAAppDelegate class]));
    }
}
