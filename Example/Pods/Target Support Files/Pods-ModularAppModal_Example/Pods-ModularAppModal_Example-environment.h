
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ModularAppCore
#define COCOAPODS_POD_AVAILABLE_ModularAppCore
#define COCOAPODS_VERSION_MAJOR_ModularAppCore 1
#define COCOAPODS_VERSION_MINOR_ModularAppCore 0
#define COCOAPODS_VERSION_PATCH_ModularAppCore 0

// ModularAppModal
#define COCOAPODS_POD_AVAILABLE_ModularAppModal
#define COCOAPODS_VERSION_MAJOR_ModularAppModal 0
#define COCOAPODS_VERSION_MINOR_ModularAppModal 1
#define COCOAPODS_VERSION_PATCH_ModularAppModal 0

