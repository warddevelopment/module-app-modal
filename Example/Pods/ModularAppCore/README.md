# ModularAppCore

[![CI Status](http://img.shields.io/travis/Wills Ward/ModularAppCore.svg?style=flat)](https://travis-ci.org/Wills Ward/ModularAppCore)
[![Version](https://img.shields.io/cocoapods/v/ModularAppCore.svg?style=flat)](http://cocoapods.org/pods/ModularAppCore)
[![License](https://img.shields.io/cocoapods/l/ModularAppCore.svg?style=flat)](http://cocoapods.org/pods/ModularAppCore)
[![Platform](https://img.shields.io/cocoapods/p/ModularAppCore.svg?style=flat)](http://cocoapods.org/pods/ModularAppCore)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ModularAppCore is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ModularAppCore"
```

## Author

Wills Ward, wward@warddevelopment.com

## License

ModularAppCore is available under the MIT license. See the LICENSE file for more info.
